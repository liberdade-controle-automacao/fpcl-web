const REFRESH_ICON = "🔄";

function generateNoteIdList(noteIndex, emptyMessage = "Start by creating another note") {
    var searchBar = document.getElementById("search-bar");
    var outlet = `
        <p>
            ${emptyMessage}
        </p>
    `;

    if (noteIndex.length > 0) {
        outlet = `<div class="row">`;
        for (var i = 0; i < noteIndex.length; i++) {
            var noteId = noteIndex[i];
            var note = getNote(noteId);
            outlet += `
                <div class="card">
                    <h5>
                        <a class="note-link" href="note.html?id=${noteId}">
                            ${note.title}
                        </a>
                    </h5>
                </div>
            `;
        }
        outlet += `</div>`;
    }

    return outlet;
}

function newNoteButtonClick() {
    var id = createNote();
    window.location.href = `./note.html?id=${id}`;
}

function syncCallback() {
    var syncButton = document.getElementById("sync-button");
    syncButton.innerHTML = `🕘`;

    downloadNotes(function(response) {
        if (!!response.error) {
            alert(response.error);
        } else {
            importNotes(response.notes);
            document.getElementById("content").innerHTML = generateNoteIdList(getNotes());
        }
        syncButton.innerHTML = REFRESH_ICON;
    });
}

function logoffCallback() {
    dropDb();
    window.location.href = `./index.html`;
}

function searchButtonClick() {
    const searchPrompt = document.getElementById("search-prompt").value.toLowerCase();
    var noteIndex = getNotes();
    var filteredNoteIndex = [];
    var searchButton = document.getElementById("search-prompt");

    searchButton.innerHTML = "Searching...";

    if ((searchPrompt === "") || (noteIndex.length === 0)) {
        document.getElementById("content").innerHTML = generateNoteIdList(noteIndex);
        searchButton.innerHTML = "Search";
        return;
    }

    for (var i = 0; i < noteIndex.length; i++) {
        var noteId = noteIndex[i];
        var note = getNote(noteId);
        var inId = noteId.toLowerCase().includes(searchPrompt);
	var inTitle = note.title.toLowerCase().includes(searchPrompt);
	var inContents = note.contents.toLowerCase().includes(searchPrompt);

        if (inId || inContents || inTitle) {
	    filteredNoteIndex.push(noteId);
	}
    }

    document.getElementById("content").innerHTML = generateNoteIdList(filteredNoteIndex, "No notes found");
    searchButton.innerHTML = "Search";
    return;
}

function setup() {
    // toolbar and database setup
    var header = document.getElementById("header");
    if (isUserLoggedIn()) {
        header.innerHTML += `
            <button type="button" name="button" id="sync-button" onclick="syncCallback()">
                ${REFRESH_ICON}
            </button>
        `;
        document.addEventListener("keyup", function(event) {
            if (event.keyCode === 119) {  // F8
                syncCallback();
            }
        });
    } else {
        initDb();
    }

    // content setup
    var noteIndex = getNotes();
    document.getElementById("content").innerHTML = generateNoteIdList(noteIndex);
    document.getElementById("new-note-button").addEventListener("click", newNoteButtonClick);
    if (noteIndex.length > 0) {
        document.getElementById("search-bar").classList.remove("hidden");
        document.getElementById("search-button").addEventListener("click", searchButtonClick);
    }
    setExistingTheme();
}
